const path = require('path');
const glob = require('glob');
const glob2base = require('glob2base');
const cwd = process.cwd();

process.on('unhandledRejection', function() {});

function prevHandle(patterns) {
    let negitive = [];
    patterns = patterns.slice();
    patterns = patterns.filter(function(pattern) {
        if (typeof pattern === 'string') {
            if (pattern[0] === '!') {
                negitive.push(pattern);
                return false;
            }
            return true;
        }
        return false;
    });
    patterns = patterns.concat(negitive);
    patterns = patterns.map(function(pattern) {
        let isPositive = pattern[0] !== '!';
        pattern = isPositive ? pattern : pattern.slice(1);
        return {
            isPositive: isPositive,
            glob: pattern
        }
    });

    return patterns;
}

function sequenceSync(gbs) {
    let result = {};
    gbs.forEach(function(gb) {
        let matchedFiles = glob.sync(gb.glob);
        let globIndtance = new glob.Glob(gb.glob);
        let basePath = glob2base(globIndtance);
        matchedFiles = matchedFiles.map(function(filename) {
            let filePath = path.resolve(cwd, filename);
            if (gb.isPositive) {
                result[filePath] = {
                    file: filename,
                    base: basePath,
                    glob: gb.glob
                };
            } else {
                delete result[filePath];
            }
        });
    });
    return Object.values(result);
}

function sequence(gbs) {
    let result = {};
    let promise = new Promise(function(resolve, reject) {
        resolve();
    });
    let theIndx = 0;
    gbs.forEach(function(gb) {
        promise = promise.then(function() {
            return new Promise(function(resolve, reject) {
                let globIndtance = new glob.Glob(gb.glob);
                let basePath = glob2base(globIndtance);
                globIndtance.on('match', function(filename) {
                    let filePath = path.resolve(cwd, filename);
                    if (gb.isPositive) {
                        result[filePath] = {
                            file: filename,
                            base: basePath,
                            glob: gb.glob
                        };
                    } else {
                        delete result[filePath];
                    }
                });
                globIndtance.on('end', function() {
                    resolve(Object.values(result));
                });
            });
        });
    });
    return promise;
}

function globs2(patterns) {
    if (typeof patterns === 'string') {
        patterns = [patterns];
    }

    let gbs = prevHandle(patterns);
    return sequence(gbs);
}

globs2.sync = function(patterns) {
    if (typeof patterns === 'string') {
        patterns = [patterns];
    }

    let gbs = prevHandle(patterns);
    return sequenceSync(gbs);
}

module.exports = globs2;