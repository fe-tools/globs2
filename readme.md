# globs2

Based to [glob](https://github.com/isaacs/node-glob), but instead of a single pattern, you may also use arrays of patterns.

### Usage

Given files:
```
files
├── src/a.js
├── src/b.js
├── src/a-html.js
└── src/b-html.js
```

globs2:
``` js
var globs2 = require('globs2');
var patterns = ['./src/*.js', '!./src/*-html.js'];
var files = globs2(patterns).then(function(res) {
    console.log(res);
});
```

globs2.sync:
``` js
var globs2 = require('globs2');
var patterns = ['./src/*.js', '!./src/*-html.js'];
var res = globs2.sync(patterns);

console.log(res);
```

Resulting in:
```
[{
    file: './src/a.js', // the matched file
    base: 'src/', // the glog base
    glob: './src/*.js' // the glob which matched the file
},{
    file: './src/b.js',
    base: 'src/',
    glob: './src/*.js'
}]
```
